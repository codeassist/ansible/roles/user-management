|Branch|Status|
|------|:--------:|
|master|[![pipeline status](https://gitlab.itsupportme.by/ansible-cm/roles/user-management/badges/master/pipeline.svg)](https://gitlab.itsupportme.by/ansible-cm/roles/user-management/commits/master)
|develop|[![pipeline status](https://gitlab.itsupportme.by/ansible-cm/roles/user-management/badges/develop/pipeline.svg)](https://gitlab.itsupportme.by/ansible-cm/roles/user-management/commits/develop)

## Ansible Role
### **_user-management_**

An Ansible role for managing user accounts on RedHat/CentOS, Debian/Ubuntu and Alpine Linux servers.

## Requirements

  - Ansible 2.5 and higher

## Role Default Variables
```yaml
## TODO
```

## Dependencies

none

## License

MIT

## Author Information

This role was created by ITSupportMe, LLC (2018)
