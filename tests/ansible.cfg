[defaults]

## Colon separated list of Ansible inventory sources
inventory = ./hosts

## Colon separated paths in which Ansible will search for Roles.
roles_path = ./roles

## Toggle Ansible logging to syslog on the target when it executes tasks.
no_target_syslog = True

## Maximum number of forks Ansible will use to execute tasks on target hosts.
forks = 30
## This is the default timeout for connection plugins to use.
timeout = 60
## For asynchronous tasks in Ansible (covered in Asynchronous Actions and Polling), this is how often to check back on
## the status of those tasks when an explicit poll interval is not supplied. The default is a reasonably moderate 15
## seconds which is a tradeoff between checking in frequently and providing a quick turnaround when something may have
## completed.
poll_interval = 5

## Set this to "False" if you want to avoid host key checking by the underlying tools Ansible uses to connect to the host
## Seems to be very important connecting to RedHat family OS. Otherwise an error occured trying to run a interactive
## command without "stdin".
host_key_checking = False

## This controls whether a failed Ansible playbook should create a .retry file.
retry_files_enabled = False


[ssh_connection]

## SSH arguments to use. Leaving 'off' ControlPersist will result in poor performance, so use paramiko on older
## platforms rather than removing it
## Due to 'https://github.com/ansible/ansible/issues/9442' - default options have been changed
ssh_args = '-C -o ControlMaster=auto -o ControlPersist=3m -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -o GlobalKnownHostsFile=/dev/null'

## Pipelining, if supported by the connection plugin, reduces the number of network operations required to execute a
## module on the remote server, by executing many Ansible modules without actual file transfer. This can result in a
## very significant performance improvement when enabled. However this conflicts with privilege escalation (become).
## For example, when using 'sudo:' operations you must first disable 'requiretty' in /etc/sudoers on all managed hosts,
## which is why it is disabled by default.
pipelining = True

# Control the mechanism for transferring files (new). If set, this will override the scp_if_ssh option
#   * sftp  = use sftp to transfer files
#   * scp   = use scp to transfer files
#   * piped = use 'dd' over SSH to transfer files
#   * smart = try sftp, scp, and piped, in that order [default]
transfer_method = smart
